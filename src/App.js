import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Feed from "./components/pages/Feed";
import Login from "./components/pages/Login";
import Signup from "./components/pages/Signup";
import Profile from "./components/pages/Profile";
import UserList from "./components/pages/UserList";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import "./interceptors";
import SearchUsers from "./components/pages/SearchUsers";
import ImageUploader from "./components/pages/ImageUploader";

function App() {
  const loggedIn = localStorage.getItem("token");
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Route exact path="/" component={loggedIn ? Feed : Login} />
          <Route exact path="/signup" component={loggedIn ? Login : Signup} />
          <Route exact path="/profile/:userID" component={Profile} />
          <Route exact path="/:userID/friends" component={UserList} />
          <Route exact path="/search" component={SearchUsers} />
          <Route exact path="/image" component={ImageUploader} />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
