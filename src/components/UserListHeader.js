import React, { Component } from "react";
import FeedLink from "./FeedLink";

class UserListHeader extends Component {
  render() {
    return (
      <nav
        className="navbar navbar-expand-lg"
        style={{ backgroundColor: "#fb5b5a" }}
      >
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <FeedLink path="/" name="FcBook" />
            <FeedLink path="/" name="Home" />
          </ul>
        </div>
      </nav>
    );
  }
}

export default UserListHeader;
