import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { addFriend, deleteFriend } from "../redux/user/actions";
import Button from "./Button";
import { Link } from "react-router-dom";

class UserListElement extends Component {
  handleDelete = (user_id) => {
    this.props.deleteFriend(user_id);
  };

  handleAdd = (user_id) => {
    this.props.addFriend(user_id);
  };

  render() {
    const { user } = this.props;
    const { loggedUser } = this.props;

    return (
      <tbody>
        <tr>
          <td>{user.firstName}</td>
          <td>{user.lastName}</td>
          <td>
            <Link className="btn btn-primary" to={`/profile/${user.id}`}>
              Go to profile
            </Link>
          </td>
          {user.isFriend ? (
            <td>
              <Button
                onClick={() => this.handleDelete(user.id)}
                name="Unfriend"
                className={
                  loggedUser.id === user.id ? "btnHidden" : "btn btn-danger"
                }
              ></Button>
            </td>
          ) : (
            <td>
              <Button
                onClick={() => this.handleAdd(user.id)}
                name="Add Friend"
                className={
                  loggedUser.id === user.id ? "btnHidden" : "btn btn-warning"
                }
              ></Button>
            </td>
          )}
        </tr>
      </tbody>
    );
  }
}

UserListElement.propTypes = {
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  loggedUser: state.user.loggedUser,
});

export default connect(mapStateToProps, {
  deleteFriend,
  addFriend,
})(UserListElement);
