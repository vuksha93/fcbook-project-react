import React, { Component } from "react";
import FeedLink from "./FeedLink";

class ProfileHeader extends Component {
  render() {
    const userID = this.props.userID;

    return (
      <nav
        className="navbar navbar-expand-lg"
        style={{ backgroundColor: "#fb5b5a" }}
      >
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <FeedLink path="/" name="FcBook" />
            <FeedLink path="/" name="Home" />
            <FeedLink path={`/${userID}/friends`} name="Friends" />
          </ul>
        </div>
      </nav>
    );
  }
}

export default ProfileHeader;
