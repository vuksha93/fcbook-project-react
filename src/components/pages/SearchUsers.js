import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { search } from "../../redux/user/actions";
import UserListElement from "../UserListElement";
import UserListHeader from "../UserListHeader";

class SearchUsers extends Component {
  componentDidMount() {
    const params = new URLSearchParams(this.props.location.search);
    const name = params.get("name");
    this.props.search(name);
  }

  render() {
    const users = this.props.users.map((user) => (
      <UserListElement key={user.id} user={user}></UserListElement>
    ));

    return (
      <React.Fragment>
        <UserListHeader></UserListHeader>
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          {users}
        </table>
      </React.Fragment>
    );
  }
}

SearchUsers.propTypes = {
  search: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.user.users,
});

export default connect(mapStateToProps, { search })(SearchUsers);
