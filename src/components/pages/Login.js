import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { authenticate } from "../../redux/auth/actions";
import InputField from "../InputField";

class Login extends Component {
  state = {
    username: "",
    password: "",
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const auth_request = {
      username: this.state.username,
      password: this.state.password,
    };

    this.props.authenticate(auth_request);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="form-login">
        <h2 className="h_name">Fc Book - Login</h2>
        <InputField
          id="username_login"
          name="username"
          placeholder="Username"
          className="form-control"
          type="text"
          required={true}
          autofocus={true}
          onChange={this.handleChange}
        />
        <InputField
          id="password_login"
          name="password"
          className="form-control"
          placeholder="Password"
          type="password"
          required={true}
          onChange={this.handleChange}
        />
        <InputField
          id="login"
          className="btn btn-lg btn-dark btn-block"
          value="LOGIN"
          type="submit"
        />
        <p className="mt-5 mb-3 text-white">Don't have an account?</p>
        <Link to="/signup"> Sign up </Link>
        <p className="mt-5 mb-3 text-white">&copy; 2020</p>
      </form>
    );
  }
}

Login.propTypes = {
  authenticate: PropTypes.func.isRequired,
};

export default connect(null, { authenticate })(Login);
