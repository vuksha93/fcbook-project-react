import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { addPost, getUserPosts } from "../../redux/post/actions";
import {
  getUser,
  addFriend,
  deleteFriend,
  getLoggedUser,
} from "../../redux/user/actions";
import Button from "../Button";
import Image from "../Image";
import InputField from "../InputField";
import Post from "../Post";
import ProfileHeader from "../ProfileHeader";

class Profile extends Component {
  state = {
    content: "",
  };

  componentDidMount() {
    const { userID } = this.props.match.params;
    this.props.getUserPosts(userID);
    this.props.getUser(userID);
    this.props.getLoggedUser();
  }

  componentDidUpdate(prevProps) {
    const { userID } = this.props.match.params;
    if (parseInt(prevProps.match.params.userID) !== parseInt(userID)) {
      this.props.getUserPosts(userID);
      this.props.getUser(userID);
    }
  }

  handleDelete = (user_id) => {
    this.props.deleteFriend(user_id);
  };

  handleAdd = (user_id) => {
    this.props.addFriend(user_id);
  };

  addPost = () => {
    const postDTO = {
      content: this.state.content,
      creationTime: new Date(),
    };

    if (postDTO.content !== "") {
      this.props.addPost(postDTO);
    }
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { loggedUser } = this.props;
    const { user } = this.props;
    const { userID } = this.props.match.params;
    const isLoggedUserProfile = loggedUser.id === parseInt(userID);

    const posts = this.props.posts.map((post) => (
      <Post key={post.id} post={post}></Post>
    ));

    return (
      <React.Fragment>
        <ProfileHeader userID={userID}></ProfileHeader>

        {isLoggedUserProfile ? (
          <div className="form-post">
            <Image></Image>
            <h3 className="headerName">
              {loggedUser.firstName} {loggedUser.lastName}
            </h3>
            <InputField
              name="content"
              className="form-control"
              type="text"
              placeholder="Add
            new post"
              onChange={this.handleChange}
            ></InputField>
            <Button
              className="btn btn-link"
              onClick={this.addPost}
              name="Add new post"
            ></Button>
            {posts}
          </div>
        ) : (
          <div className="form-post">
            <Image></Image>
            <h3 className="headerName">
              {user.firstName} {user.lastName}
            </h3>
            {/* TODO */}
            {/* <Button
              id="profile-button"
              className={user.isFriend ? "btn btn-danger" : "btn btn-warning"}
              name={user.isFriend ? "Unfriend" : "Add friend"}
              onClick={() => {
                user.isFriend
                  ? this.handleDelete(user.id)
                  : this.handleAdd(user.id);
              }}
            ></Button> */}
            {posts}
          </div>
        )}
      </React.Fragment>
    );
  }
}

Profile.propTypes = {
  getUserPosts: PropTypes.func.isRequired,
  getLoggedUser: PropTypes.func.isRequired,
  loggedUser: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  posts: state.post.posts,
  loggedUser: state.user.loggedUser,
  user: state.user.user,
});

export default connect(mapStateToProps, {
  addPost,
  getUserPosts,
  getUser,
  addFriend,
  deleteFriend,
  getLoggedUser,
})(Profile);
