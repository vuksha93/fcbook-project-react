import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { registration } from "../../redux/auth/actions";
import InputField from "../InputField";

class Signup extends Component {
  state = {
    username: "",
    password: "",
    repeatedPassword: "",
    email: "",
    firstName: "",
    lastName: "",
    yearOfBirth: "",
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const userRegistrationDTO = {
      username: this.state.username,
      password: this.state.password,
      repeatedPassword: this.state.repeatedPassword,
      email: this.state.email,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      yearOfBirth: this.state.yearOfBirth,
    };

    this.props.registration(userRegistrationDTO, this.props.history);
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="form-signup">
        <h2 className="h_name">Fc Book - Signup</h2>
        <InputField
          id="username_signup"
          name="username"
          className="form-control"
          placeholder="Username"
          type="text"
          required={true}
          autofocus={true}
          onChange={this.handleChange}
        />
        <InputField
          id="password_signup"
          name="password"
          className="form-control"
          placeholder="Password"
          type="password"
          required={true}
          onChange={this.handleChange}
        />
        <InputField
          id="repeatedPassword"
          name="repeatedPassword"
          className="form-control"
          placeholder="Repeated Password"
          type="password"
          required={true}
          onChange={this.handleChange}
        />
        <InputField
          id="email"
          name="email"
          className="form-control"
          placeholder="Email"
          type="email"
          required={true}
          onChange={this.handleChange}
        />
        <InputField
          id="firstName"
          name="firstName"
          className="form-control"
          placeholder="First Name"
          type="text"
          required={true}
          onChange={this.handleChange}
        />
        <InputField
          id="lastName"
          name="lastName"
          className="form-control"
          placeholder="Last Name"
          type="text"
          required={true}
          onChange={this.handleChange}
        />
        <InputField
          id="yearOfBirth"
          name="yearOfBirth"
          className="form-control"
          placeholder="Birth Year"
          type="number"
          min="1900"
          max="2002"
          required={true}
          onChange={this.handleChange}
        />

        <InputField
          id="signup"
          className="btn btn-lg btn-dark btn-block"
          value="SIGN UP"
          type="submit"
        />

        <p className="mt-5 mb-3 text-white">&copy; 2020</p>
      </form>
    );
  }
}

Signup.propTypes = {
  registration: PropTypes.func.isRequired,
};

export default connect(null, { registration })(Signup);
