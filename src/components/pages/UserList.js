import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { getUserFriends } from "../../redux/user/actions";
import UserListElement from "../UserListElement";
import UserListHeader from "../UserListHeader";

class UserList extends Component {
  componentDidMount() {
    const { userID } = this.props.match.params;
    this.props.getUserFriends(userID);
  }

  render() {
    const users = this.props.users.map((user) => (
      <UserListElement key={user.id} user={user}></UserListElement>
    ));

    return (
      <React.Fragment>
        <UserListHeader></UserListHeader>
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col"></th>
              <th scope="col"></th>
            </tr>
          </thead>
          {users}
        </table>
      </React.Fragment>
    );
  }
}

UserList.propTypes = {
  getUserFriends: PropTypes.func.isRequired,
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  friends: state.user.friends,
  users: state.user.users,
  loggedUser: state.user.loggedUser,
});

export default connect(mapStateToProps, { getUserFriends })(UserList);
