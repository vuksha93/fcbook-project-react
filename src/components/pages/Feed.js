import React, { Component } from "react";
import FeedHeader from "../FeedHeader";
import Post from "../Post";
import { connect } from "react-redux";
import { getFeed } from "../../redux/post/actions";
import { addPost } from "../../redux/post/actions";
import { getLoggedUser } from "../../redux/user/actions";
import InputField from "../InputField";
import Button from "../Button";
import PropTypes from "prop-types";
import Image from "../Image";

class Feed extends Component {
  state = {
    content: "",
  };

  componentDidMount() {
    this.props.getFeed();
    this.props.getLoggedUser();
  }

  addPost = (event) => {
    event.preventDefault();
    const postDTO = {
      content: this.state.content,
      creationTime: new Date(),
    };

    if (postDTO.content !== "") {
      this.props.addPost(postDTO);
    }
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { loggedUser } = this.props;
    const posts = this.props.posts.map((post) => (
      <Post key={post.id} post={post}></Post>
    ));
    return (
      <React.Fragment>
        <FeedHeader></FeedHeader>
        <div className="form-post">
          <Image></Image>
          <h3 className="headerName">
            {loggedUser.firstName} {loggedUser.lastName}
          </h3>
          <InputField
            name="content"
            className="form-control"
            type="text"
            placeholder="Add new post"
            onChange={this.handleChange}
          />
          <Button
            className="btn btn-link"
            onClick={this.addPost}
            name="Add new post"
          />
          {posts}
        </div>
      </React.Fragment>
    );
  }
}

Feed.propTypes = {
  getLoggedUser: PropTypes.func.isRequired,
  getFeed: PropTypes.func.isRequired,
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  posts: state.post.posts,
  loggedUser: state.user.loggedUser,
});

export default connect(mapStateToProps, { getFeed, addPost, getLoggedUser })(
  Feed
);
