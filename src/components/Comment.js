import React, { Component } from "react";
import Button from "./Button";
import InputField from "./InputField";
import { connect } from "react-redux";
import {
  addNewComment,
  deleteComment,
  editComment,
} from "./../redux/comment/actions";
import { Link } from "react-router-dom";

class Comment extends Component {
  state = {
    input: "",
    showInput: false,
    editArea: "",
    showTextArea: false,
    content: "",
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleKeyPress = (event, post_id, comment_id) => {
    const { comment } = this.props;
    comment_id = comment.id;
    post_id = comment.postDTO.id;
    const commentDTO = {
      content: this.state.content,
    };

    if (event.key === "Enter") {
      if (commentDTO.content !== "") {
        this.props.addNewComment(post_id, commentDTO, comment_id);
      }
    }
  };

  handleAddReply = (post_id, comment_id) => {
    const { comment } = this.props;
    comment_id = comment.id;
    post_id = comment.postDTO.id;
    const commentDTO = {
      content: this.state.content,
    };

    if (commentDTO.content !== "") {
      this.props.addNewComment(post_id, commentDTO, comment_id);
    }
  };

  handleEdit = (comment_id) => {
    const { comment } = this.props;
    comment_id = comment.id;
    const commentDTO = {
      content: this.state.content,
    };

    if (commentDTO.content !== "") {
      this.props.editComment(comment_id, commentDTO);
    }
  };

  getEdit = () => {
    this.setState({
      showTextArea: !this.state.showTextArea,
      editArea: (
        <React.Fragment>
          <InputField
            name="content"
            className="form-control"
            type="text"
            placeholder="Edit comment"
            onChange={this.handleChange}
          ></InputField>
          <Button
            onClick={this.handleEdit}
            className="btn btn-link"
            name="Edit"
          ></Button>
        </React.Fragment>
      ),
    });
  };

  handleDelete = (comment_id) => {
    const { comment } = this.props;
    comment_id = comment.id;
    this.props.deleteComment(comment_id);
  };

  reply = () => {
    this.setState({
      showInput: !this.state.showInput,
      input: (
        <React.Fragment>
          <InputField
            name="content"
            className="form-control"
            type="text"
            placeholder="Add new reply"
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
          ></InputField>
          <Button
            onClick={this.handleAddReply}
            className="btn btn-link"
            name="Add"
          ></Button>
        </React.Fragment>
      ),
    });
  };

  render() {
    const { comment } = this.props;
    const { comments } = this.props;
    const { loggedUser } = this.props;

    const replies = (comments[comment.postDTO.id] || []).filter(
      (reply) => reply.parentCommentID === comment.id
    );
    const postReplies = replies.map((comment) => (
      <Comment {...this.props} key={comment.id} comment={comment}></Comment>
    ));

    return (
      <span className="border" id="textarea">
        <Link id="userData" to={`/profile/${comment.userDTO.id}`}>
          {comment.userDTO.firstName} {comment.userDTO.lastName}
        </Link>
        <br></br>
        {comment.content}
        {postReplies}
        {loggedUser.id === comment.userDTO.id ||
        loggedUser.id === comment.postDTO.userDTO.id ? (
          <div>
            <Button
              className="btn btn-link"
              onClick={this.handleDelete}
              name="Delete Comment"
            ></Button>
          </div>
        ) : (
          ""
        )}

        {loggedUser.id === comment.userDTO.id ? (
          <div>
            <Button
              className="btn btn-link"
              onClick={this.getEdit}
              name={this.state.showTextArea ? "Hide" : "Edit Comment"}
            ></Button>
            {this.state.showTextArea ? this.state.editArea : ""}
          </div>
        ) : (
          ""
        )}

        <div>
          <Button
            className="btn btn-link"
            onClick={this.reply}
            name={this.state.showInput ? "Hide" : "Reply"}
          ></Button>
          {this.state.showInput ? this.state.input : ""}
        </div>
      </span>
    );
  }
}

const mapStateToProps = (state) => ({
  comments: state.comment.comments,
  loggedUser: state.user.loggedUser,
});

export default connect(mapStateToProps, {
  addNewComment,
  deleteComment,
  editComment,
})(Comment);
