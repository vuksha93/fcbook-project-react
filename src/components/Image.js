import React, { Component } from "react";
import { Link } from "react-router-dom";
import userPhoto from "./../images/SpidermanAvatar.png";

class Image extends Component {
  render() {
    return (
      <Link className="img_button" title="Change photo" to="/image">
        <img className="userPhoto" alt="Profile" src={userPhoto}></img>
      </Link>
    );
  }
}

export default Image;
