import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import Button from "./Button";
import InputField from "./InputField";
import { deletePost, editPost } from "./../redux/post/actions";
import { getComments, addNewComment } from "./../redux/comment/actions";
import { Link } from "react-router-dom";
import Comment from "./Comment";

class Post extends Component {
  state = {
    showComments: false,
    content: "",
    showTextArea: false,
    editTextArea: "",
  };

  handleComments = (post_id) => {
    const { post } = this.props;
    post_id = post.id;
    this.props.getComments(post_id);
    this.setState({ showComments: true });
  };

  hideComments = () => {
    this.setState({ showComments: false });
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  addNewComment = (post_id) => {
    const { post } = this.props;
    post_id = post.id;
    const commentDTO = {
      content: this.state.content,
    };

    if (commentDTO.content !== "") {
      this.props.addNewComment(post_id, commentDTO);
    }
  };

  handleDelete = (post_id) => {
    const { post } = this.props;
    post_id = post.id;
    this.props.deletePost(post_id);
  };

  handleEdit = (post_id) => {
    const { post } = this.props;
    post_id = post.id;
    const commentDTO = { content: this.state.content };

    if (commentDTO.content !== "") {
      this.props.editPost(post_id, commentDTO);
    }
  };

  getEdit = () => {
    this.setState({
      showTextArea: !this.state.showTextArea,
      editTextArea: (
        <React.Fragment>
          <InputField
            name="content"
            className="form-control"
            type="text"
            placeholder="Edit post"
            onChange={this.handleChange}
          ></InputField>
          <Button
            onClick={this.handleEdit}
            className="btn btn-link"
            name="Edit"
          ></Button>
        </React.Fragment>
      ),
    });
  };

  render() {
    const { post, comments, loggedUser } = this.props;
    let showComments = this.state.showComments;

    const parentComments = comments[post.id]
      ? comments[post.id].filter((comment) => comment.parentCommentID === 0)
      : [];

    const postComments =
      parentComments && showComments
        ? parentComments.map((comment) => (
            <Comment key={comment.id} comment={comment}></Comment>
          ))
        : [];

    return (
      <React.Fragment>
        <label className="postContent">
          <Link to={`/profile/${post.userDTO.id}`}>
            {post.userDTO.firstName} {post.userDTO.lastName}
          </Link>
          <div style={{ color: "white" }}>{post.creationTime}</div>
        </label>
        <span className="border" id="textarea" rows="3">
          {post.content}
          {postComments}
        </span>

        {post.userDTO.id === loggedUser.id ? (
          <React.Fragment>
            <Button
              onClick={this.handleDelete}
              className="btn btn-link"
              name="Delete"
            ></Button>
            <Button
              onClick={this.getEdit}
              className="btn btn-link"
              name={this.state.showTextArea ? "Hide Edit" : "Edit"}
            ></Button>
            {this.state.showTextArea ? this.state.editTextArea : ""}
          </React.Fragment>
        ) : (
          ""
        )}

        {!showComments ? (
          <Button
            className="btn btn-link"
            onClick={() => this.handleComments(post.id)}
            name="Get Comments"
          />
        ) : (
          <Button
            className="btn btn-link"
            onClick={this.hideComments}
            name="Hide Comments"
          />
        )}

        <InputField
          name="content"
          className="form-control"
          type="text"
          placeholder="Add new comment"
          onChange={this.handleChange}
        />
        <Button
          className="btn btn-link"
          onClick={() => this.addNewComment(post.id)}
          name="Add comment"
        />
      </React.Fragment>
    );
  }
}

Post.propTypes = {
  getComments: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  comments: state.comment.comments,
  loggedUser: state.user.loggedUser,
});

export default connect(mapStateToProps, {
  getComments,
  addNewComment,
  deletePost,
  editPost,
})(Post);
