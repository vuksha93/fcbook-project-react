import React, { Component } from "react";
import { Link } from "react-router-dom";

class FeedLink extends Component {
  render() {
    return (
      <li className="nav-item">
        <Link className="nav-link" to={this.props.path}>
          {this.props.name}
        </Link>
      </li>
    );
  }
}

export default FeedLink;
