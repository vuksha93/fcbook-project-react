import PropTypes from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { logout } from "./../redux/auth/actions";
import { search, deleteUser } from "./../redux/user/actions";
import FeedLink from "./FeedLink";
import InputField from "./InputField";
import Button from "./Button";

class FeedHeader extends Component {
  state = {
    name: "",
  };

  handleChange = (event) => {
    this.setState({ name: event.target.value });
  };

  handleClick = (event) => {
    event.preventDefault();
    this.props.logout();
  };

  handleDelete = (userID) => {
    const { loggedUser } = this.props;
    userID = loggedUser.id;
    this.props.deleteUser(userID);
  };

  render() {
    const { loggedUser } = this.props;
    return (
      <nav
        className="navbar navbar-expand-lg"
        style={{ backgroundColor: "#fb5b5a" }}
      >
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <FeedLink path="/" name="FcBook" />
            <FeedLink path="/" name="Home" />
            <FeedLink path={`/profile/${loggedUser.id}`} name="Profile" />

            <li className="nav-item active">
              <Button
                id="btn_logout"
                className="btn btn-dark"
                onClick={this.handleClick}
                name="Log Out"
              ></Button>

              <Button
                id="btn_deleteAccount"
                className="btn btn-dark"
                onClick={() => this.handleDelete(loggedUser.id)}
                name="Delete Account"
              ></Button>
            </li>
          </ul>

          <form className="form-inline my-2 my-md-0">
            <InputField
              name="search"
              className="form-control"
              placeholder="Search friends"
              type="search"
              onChange={this.handleChange}
            />
            <ul className="navbar-nav mr-auto">
              <FeedLink
                path={`/search?name=${this.state.name}`}
                name="Search"
              ></FeedLink>
            </ul>
          </form>
        </div>
      </nav>
    );
  }
}

FeedHeader.propTypes = {
  loggedUser: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  loggedUser: state.user.loggedUser,
});

export default connect(mapStateToProps, { deleteUser, logout, search })(
  FeedHeader
);
