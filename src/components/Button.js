import React, { Component } from "react";

class Button extends Component {
  render() {
    return (
      <button
        onClick={this.props.onClick}
        className={this.props.className}
        id={this.props.id}
      >
        {this.props.name}
      </button>
    );
  }
}

export default Button;
