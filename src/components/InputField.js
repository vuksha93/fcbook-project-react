import React, { Component } from "react";

class InputField extends Component {
  render() {
    return (
      <input
        id={this.props.id}
        type={this.props.type}
        name={this.props.name}
        className={this.props.className}
        placeholder={this.props.placeholder}
        required={this.props.required}
        autoFocus={this.props.autofocus}
        min={this.props.min}
        max={this.props.max}
        value={this.props.value}
        onChange={this.props.onChange}
        onKeyPress={this.props.onKeyPress}
      />
    );
  }
}

export default InputField;
