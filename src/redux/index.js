import { combineReducers } from "redux";
import authReducer from "./auth/reducer";
import userReducer from "./user/reducer";
import postReducer from "./post/reducer";
import commentReducer from "./comment/reducer";

export default combineReducers({
  authentication: authReducer,
  user: userReducer,
  post: postReducer,
  comment: commentReducer,
});
