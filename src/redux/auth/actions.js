import axios from "axios";

import {
  AUTHENTICATE_START,
  AUTHENTICATE_SUCCESS,
  AUTHENTICATE_ERROR,
  REGISTRATION_START,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
  LOGOUT,
} from "./types";

export const authenticate = (auth_request) => async (dispatch) => {
  dispatch({
    type: AUTHENTICATE_START,
  });

  try {
    const res = await axios.post(
      "http://localhost:8080/authenticate",
      auth_request
    );
    dispatch({
      type: AUTHENTICATE_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: AUTHENTICATE_ERROR,
      payload: error,
    });
  }
};

export const registration = (userRegistrationDTO, history) => async (
  dispatch
) => {
  dispatch({
    type: REGISTRATION_START,
  });

  try {
    await axios.post("http://localhost:8080/registration", userRegistrationDTO);
    history.push("/");
    dispatch({
      type: REGISTRATION_SUCCESS,
      payload: userRegistrationDTO,
    });
  } catch (error) {
    dispatch({
      type: REGISTRATION_ERROR,
      payload: error,
    });
  }
};

export const logout = () => (dispatch) => {
  dispatch({
    type: LOGOUT,
  });
};
