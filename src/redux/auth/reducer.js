import {
  AUTHENTICATE_START,
  AUTHENTICATE_SUCCESS,
  AUTHENTICATE_ERROR,
  REGISTRATION_START,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
  LOGOUT,
} from "./types";

const initialState = {
  start: false,
  token: "",
  error: null,
  userRegistrationDTO: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case AUTHENTICATE_START:
      return {
        ...state,
        start: true,
      };
    case AUTHENTICATE_SUCCESS:
      localStorage.setItem("token", action.payload);
      window.location.reload();
      return {
        ...state,
        start: false,
        token: action.payload,
      };
    case AUTHENTICATE_ERROR:
      return {
        ...state,
        start: false,
        error: action.payload,
      };
    case REGISTRATION_START:
      return {
        ...state,
        start: true,
      };
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        start: false,
        userRegistrationDTO: action.payload,
      };
    case REGISTRATION_ERROR:
      return { ...state, start: false, error: action.payload };

    case LOGOUT:
      localStorage.removeItem("token");
      window.location.reload();
      return { ...state, token: null };

    default:
      return state;
  }
}
