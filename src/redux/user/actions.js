import axios from "axios";

import {
  DELETE_FRIEND_START,
  DELETE_FRIEND_SUCCESS,
  DELETE_FRIEND_ERROR,
  GET_LOGGED_USER_START,
  GET_LOGGED_USER_SUCCESS,
  GET_LOGGED_USER_ERROR,
  SEARCH_START,
  SEARCH_SUCCESS,
  SEARCH_ERROR,
  ADD_FRIEND_START,
  ADD_FRIEND_SUCCESS,
  ADD_FRIEND_ERROR,
  GET_USER_FRIENDS_START,
  GET_USER_FRIENDS_SUCCESS,
  GET_USER_FRIENDS_ERROR,
  GET_USER_START,
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  DELETE_USER_START,
  DELETE_USER_SUCCESS,
  DELETE_USER_ERROR,
  UPLOAD_IMAGE_START,
  UPLOAD_IMAGE_SUCCESS,
  UPLOAD_IMAGE_ERROR,
} from "./types";

const url = `http://localhost:8080`;

export const getUserFriends = (userID) => async (dispatch) => {
  dispatch({
    type: GET_USER_FRIENDS_START,
  });

  try {
    const res = await axios.get(url + `/${userID}/friends`);
    dispatch({
      type: GET_USER_FRIENDS_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_USER_FRIENDS_ERROR,
      payload: error,
    });
  }
};

export const deleteFriend = (friendID) => async (dispatch) => {
  if (
    window.confirm(
      `You are deleting friend ${friendID}, this action cannot be undone!`
    )
  ) {
    dispatch({
      type: DELETE_FRIEND_START,
    });

    try {
      await axios.delete(url + `/deletefriend/${friendID}`);
      dispatch({
        type: DELETE_FRIEND_SUCCESS,
        payload: friendID,
      });
    } catch (error) {
      dispatch({
        type: DELETE_FRIEND_ERROR,
        payload: error,
      });
    }
  }
};

export const getLoggedUser = () => async (dispatch) => {
  dispatch({
    type: GET_LOGGED_USER_START,
  });

  try {
    const res = await axios.get(url + "/user");
    dispatch({
      type: GET_LOGGED_USER_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_LOGGED_USER_ERROR,
      payload: error,
    });
  }
};

export const getUser = (userID) => async (dispatch) => {
  dispatch({
    type: GET_USER_START,
  });

  try {
    const res = await axios.get(url + `/${userID}`);
    dispatch({
      type: GET_USER_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_USER_ERROR,
      payload: error,
    });
  }
};

export const search = (name) => async (dispatch) => {
  dispatch({
    type: SEARCH_START,
  });

  try {
    const res = await axios.get(url + "/search", { params: { name } });
    dispatch({
      type: SEARCH_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: SEARCH_ERROR,
      payload: error,
    });
  }
};

export const addFriend = (friendID) => async (dispatch) => {
  if (window.confirm(`You are adding friend ${friendID}!`)) {
    dispatch({
      type: ADD_FRIEND_START,
    });

    try {
      await axios.post(url + `/addfriend/${friendID}`);
      dispatch({
        type: ADD_FRIEND_SUCCESS,
        payload: friendID,
      });
    } catch (error) {
      dispatch({
        type: ADD_FRIEND_ERROR,
        payload: error,
      });
    }
  }
};

export const deleteUser = (userID) => async (dispatch) => {
  if (window.confirm("You are gonna deactivate your account!")) {
    dispatch({
      type: DELETE_USER_START,
    });

    try {
      await axios.delete(url + `/${userID}`);
      dispatch({
        type: DELETE_USER_SUCCESS,
        payload: userID,
      });
    } catch (error) {
      dispatch({
        type: DELETE_USER_ERROR,
        payload: error,
      });
    }
  }
};

export const uploadImage = (imageData) => async (dispatch) => {
  dispatch({
    type: UPLOAD_IMAGE_START,
  });
  try {
    if (imageData.entries().next().value[1] !== null) {
      const res = await axios.post(url + "/image", imageData, {
        onUploadProgress: (progressEvent) => {
          console.log(
            "Uploading : " +
              ((progressEvent.loaded / progressEvent.total) * 100).toString() +
              "%"
          );
        },
      });
      dispatch({
        type: UPLOAD_IMAGE_SUCCESS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: UPLOAD_IMAGE_ERROR,
      payload: error,
    });
  }
};
