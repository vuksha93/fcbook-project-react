import {
  DELETE_FRIEND_START,
  DELETE_FRIEND_SUCCESS,
  DELETE_FRIEND_ERROR,
  GET_LOGGED_USER_START,
  GET_LOGGED_USER_SUCCESS,
  GET_LOGGED_USER_ERROR,
  SEARCH_START,
  SEARCH_SUCCESS,
  SEARCH_ERROR,
  ADD_FRIEND_START,
  ADD_FRIEND_SUCCESS,
  ADD_FRIEND_ERROR,
  GET_USER_FRIENDS_START,
  GET_USER_FRIENDS_SUCCESS,
  GET_USER_FRIENDS_ERROR,
  GET_USER_START,
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  DELETE_USER_START,
  DELETE_USER_SUCCESS,
  DELETE_USER_ERROR,
  UPLOAD_IMAGE_START,
  UPLOAD_IMAGE_SUCCESS,
  UPLOAD_IMAGE_ERROR,
} from "./types";

const initialState = {
  start: false,
  posts: [],
  error: null,
  loggedUser: {},
  user: {},
  users: [],
  image: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_USER_FRIENDS_START:
      return { ...state, start: true };
    case GET_USER_FRIENDS_SUCCESS:
      return {
        ...state,
        start: false,
        users: action.payload,
      };
    case GET_USER_FRIENDS_ERROR:
      return { ...state, start: false, error: action.payload };
    case DELETE_FRIEND_START:
      return { ...state, start: true };
    case DELETE_FRIEND_SUCCESS:
      const users = state.users;
      const deletedUser = users.find((user) => user.id === action.payload);
      const cloneUser = { ...deletedUser };
      cloneUser.isFriend = false;
      users.splice(users.indexOf(deletedUser), 1, cloneUser);
      const usersAfterDelete = users.slice();
      return {
        ...state,
        start: false,
        users: usersAfterDelete,
        user: cloneUser,
      };
    case DELETE_FRIEND_ERROR:
      return { ...state, start: false, error: action.payload };
    case GET_LOGGED_USER_START:
      return { ...state, start: true };
    case GET_LOGGED_USER_SUCCESS:
      return { ...state, start: false, loggedUser: action.payload };
    case GET_LOGGED_USER_ERROR:
      return { ...state, start: false, error: action.payload };
    case GET_USER_START:
      return { ...state, start: true };
    case GET_USER_SUCCESS:
      return { ...state, start: false, user: action.payload };
    case GET_USER_ERROR:
      return { ...state, start: false, error: action.payload };
    case SEARCH_START:
      return { ...state, start: true };
    case SEARCH_SUCCESS:
      return { ...state, start: false, users: action.payload };
    case SEARCH_ERROR:
      return { ...state, start: false, error: action.payload };
    case ADD_FRIEND_START:
      return { ...state, start: true };
    case ADD_FRIEND_SUCCESS:
      const usersAdd = state.users;
      const addedUser = usersAdd.find((user) => user.id === action.payload);
      const cloneAddedUser = { ...addedUser };
      cloneAddedUser.isFriend = true;
      usersAdd.splice(usersAdd.indexOf(addedUser), 1, cloneAddedUser);
      const usersAfterAdd = usersAdd.slice();
      return {
        ...state,
        start: false,
        user: cloneAddedUser,
        users: usersAfterAdd,
      };
    case ADD_FRIEND_ERROR:
      return { ...state, start: false, error: action.payload };
    case DELETE_USER_START:
      return { ...state, start: true };
    case DELETE_USER_SUCCESS:
      localStorage.removeItem("token");
      window.location.reload();
      return { ...state, start: false };
    case DELETE_USER_ERROR:
      return { ...state, start: false, error: action.payload };
    case UPLOAD_IMAGE_START:
      return { ...state, start: true };
    case UPLOAD_IMAGE_SUCCESS:
      return { ...state, start: false, image: action.payload };
    case UPLOAD_IMAGE_ERROR:
      return { ...state, start: false, error: action.payload };
    default:
      return state;
  }
}
