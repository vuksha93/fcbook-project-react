import axios from "axios";

import {
  GET_COMMENTS_START,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_ERROR,
  ADD_NEW_COMMENT_START,
  ADD_NEW_COMMENT_SUCCESS,
  ADD_NEW_COMMENT_ERROR,
  DELETE_COMMENT_START,
  DELETE_COMMENT_SUCCESS,
  DELETE_COMMENT_ERROR,
  EDIT_COMMENT_START,
  EDIT_COMMENT_SUCCESS,
  EDIT_COMMENT_ERROR,
} from "./types";

const url = `http://localhost:8080`;

export const getComments = (post_id) => async (dispatch) => {
  dispatch({
    type: GET_COMMENTS_START,
  });

  try {
    const res = await axios.get(url + `/posts/${post_id}/comments`);
    dispatch({
      type: GET_COMMENTS_SUCCESS,
      payload: res.data,
      post_id,
    });
  } catch (error) {
    dispatch({
      type: GET_COMMENTS_ERROR,
      payload: error,
    });
  }
};

export const addNewComment = (
  post_id,
  commentCreationDTO,
  comment_id = ""
) => async (dispatch) => {
  dispatch({
    type: ADD_NEW_COMMENT_START,
  });

  try {
    const res = await axios.post(
      url + `/posts/${post_id}/addcomment/${comment_id}`,
      commentCreationDTO
    );
    dispatch({
      type: ADD_NEW_COMMENT_SUCCESS,
      payload: res.data,
      post_id,
    });
  } catch (error) {
    dispatch({
      type: ADD_NEW_COMMENT_ERROR,
      payload: error,
    });
  }
};

export const deleteComment = (comment_id) => async (dispatch) => {
  dispatch({
    type: DELETE_COMMENT_START,
  });

  try {
    const res = await axios.delete(url + `/comments/${comment_id}`);
    dispatch({
      type: DELETE_COMMENT_SUCCESS,
      payload: res.data,
      comment_id,
    });
  } catch (error) {
    dispatch({
      type: DELETE_COMMENT_ERROR,
      payload: error,
    });
  }
};

export const editComment = (comment_id, commentCreationDTO) => async (
  dispatch
) => {
  dispatch({
    type: EDIT_COMMENT_START,
  });

  try {
    const res = await axios.put(
      url + `/comments/${comment_id}`,
      commentCreationDTO
    );
    dispatch({
      type: EDIT_COMMENT_SUCCESS,
      payload: res.data,
      comment_id,
    });
  } catch (error) {
    dispatch({
      type: EDIT_COMMENT_ERROR,
      payload: error,
    });
  }
};
