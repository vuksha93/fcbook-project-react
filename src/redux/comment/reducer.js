import {
  GET_COMMENTS_START,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_ERROR,
  ADD_NEW_COMMENT_START,
  ADD_NEW_COMMENT_SUCCESS,
  ADD_NEW_COMMENT_ERROR,
  DELETE_COMMENT_START,
  DELETE_COMMENT_SUCCESS,
  DELETE_COMMENT_ERROR,
  EDIT_COMMENT_START,
  EDIT_COMMENT_SUCCESS,
  EDIT_COMMENT_ERROR,
} from "./types";

const initialState = { start: false, error: null, comments: {} };

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_COMMENTS_START:
      return {
        ...state,
        start: true,
      };
    case GET_COMMENTS_SUCCESS:
      const comments = { ...state.comments };
      comments[action.post_id] = action.payload;
      return {
        ...state,
        start: false,
        comments,
      };
    case GET_COMMENTS_ERROR:
      return {
        ...state,
        start: false,
        error: action.payload,
      };
    case ADD_NEW_COMMENT_START:
      return { ...state, start: true };
    case ADD_NEW_COMMENT_SUCCESS:
      const newComments = { ...state.comments };
      const cloneComments = newComments[action.post_id];
      newComments[action.post_id] = [...cloneComments, action.payload];
      return { ...state, start: false, comments: newComments };
    case ADD_NEW_COMMENT_ERROR:
      return { ...state, start: false, error: action.payload };
    case DELETE_COMMENT_START:
      return { ...state, start: true };
    case DELETE_COMMENT_SUCCESS:
      const commentsDelete = { ...state.comments };
      const postCommentsDelete = commentsDelete[action.payload.postDTO.id];
      commentsDelete[action.payload.postDTO.id] = postCommentsDelete.filter(
        (comment) => comment.id !== action.comment_id
      );
      return { ...state, start: false, comments: commentsDelete };
    case DELETE_COMMENT_ERROR:
      return { ...state, start: false, error: action.payload };
    case EDIT_COMMENT_START:
      return { ...state, start: true };
    case EDIT_COMMENT_SUCCESS:
      const commentsEdit = { ...state.comments };
      const postCommentsEdit = commentsEdit[action.payload.postDTO.id];
      const editedComment = postCommentsEdit.find(
        (comment) => comment.id === action.comment_id
      );
      const cloneComment = { ...editedComment };
      cloneComment.content = action.payload.content;
      postCommentsEdit.splice(
        postCommentsEdit.indexOf(editedComment),
        1,
        cloneComment
      );
      return { ...state, start: false, comments: commentsEdit };
    case EDIT_COMMENT_ERROR:
      return { ...state, start: false, error: action.payload };
    default:
      return state;
  }
}
