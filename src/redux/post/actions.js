import axios from "axios";

import {
  FEED_START,
  FEED_SUCCESS,
  FEED_ERROR,
  GET_USER_POSTS_START,
  GET_USER_POSTS_SUCCESS,
  GET_USER_POSTS_ERROR,
  ADD_POST_START,
  ADD_POST_SUCCESS,
  ADD_POST_ERROR,
  DELETE_POST_START,
  DELETE_POST_SUCCESS,
  DELETE_POST_ERROR,
  EDIT_POST_START,
  EDIT_POST_SUCCESS,
  EDIT_POST_ERROR,
} from "./types";

const url = `http://localhost:8080/posts`;

export const getFeed = () => async (dispatch) => {
  dispatch({
    type: FEED_START,
  });

  try {
    const res = await axios.get(url + "/feed");
    dispatch({
      type: FEED_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: FEED_ERROR,
      payload: error,
    });
  }
};

export const getUserPosts = (userID) => async (dispatch) => {
  dispatch({
    type: GET_USER_POSTS_START,
  });
  try {
    const res = await axios.get(url + `/${userID}`);
    dispatch({
      type: GET_USER_POSTS_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_USER_POSTS_ERROR,
      payload: error,
    });
  }
};

export const addPost = (postCreationDTO) => async (dispatch) => {
  dispatch({
    type: ADD_POST_START,
  });

  try {
    const res = await axios.post(url, postCreationDTO);
    dispatch({
      type: ADD_POST_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: ADD_POST_ERROR,
      payload: error,
    });
  }
};

export const deletePost = (post_id) => async (dispatch) => {
  dispatch({
    type: DELETE_POST_START,
  });

  try {
    await axios.delete(url + `/${post_id}`);
    dispatch({
      type: DELETE_POST_SUCCESS,
      payload: post_id,
    });
  } catch (error) {
    dispatch({
      type: DELETE_POST_ERROR,
      payload: error,
    });
  }
};

export const editPost = (post_id, postCreationDTO) => async (dispatch) => {
  dispatch({
    type: EDIT_POST_START,
  });

  try {
    await axios.put(url + `/${post_id}`, postCreationDTO);
    dispatch({
      type: EDIT_POST_SUCCESS,
      post_id,
      postCreationDTO,
    });
  } catch (error) {
    dispatch({
      type: EDIT_POST_ERROR,
      payload: error,
    });
  }
};
