import {
  FEED_START,
  FEED_SUCCESS,
  FEED_ERROR,
  GET_USER_POSTS_START,
  GET_USER_POSTS_SUCCESS,
  GET_USER_POSTS_ERROR,
  ADD_POST_START,
  ADD_POST_SUCCESS,
  ADD_POST_ERROR,
  DELETE_POST_START,
  DELETE_POST_SUCCESS,
  DELETE_POST_ERROR,
  EDIT_POST_START,
  EDIT_POST_SUCCESS,
  EDIT_POST_ERROR,
} from "./types";

const initialState = {
  start: false,
  error: null,
  posts: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FEED_START:
      return { ...state, start: true };
    case FEED_SUCCESS:
      return { ...state, start: false, posts: action.payload };
    case FEED_ERROR:
      return { ...state, start: false, error: action.payload };
    case GET_USER_POSTS_START:
      return { ...state, start: true };
    case GET_USER_POSTS_SUCCESS:
      return { ...state, start: false, posts: action.payload };
    case GET_USER_POSTS_ERROR:
      return { ...state, start: false, error: action.payload };
    case ADD_POST_START:
      return {
        ...state,
        start: true,
      };
    case ADD_POST_SUCCESS:
      return {
        ...state,
        start: false,
        posts: [action.payload, ...state.posts],
      };
    case ADD_POST_ERROR:
      return { ...state, start: false, error: action.payload };
    case DELETE_POST_START:
      return { ...state, start: true };
    case DELETE_POST_SUCCESS:
      const posts = state.posts;
      const postsAfterDelete = posts.filter(
        (post) => post.id !== action.payload
      );
      return { ...state, start: false, posts: postsAfterDelete };
    case DELETE_POST_ERROR:
      return { ...state, start: false, error: action.payload };
    case EDIT_POST_START:
      return { ...state, start: true };
    case EDIT_POST_SUCCESS:
      const editPosts = state.posts;
      const editedPost = editPosts.find((post) => post.id === action.post_id);
      const clonePost = { ...editedPost };
      clonePost.content = action.postCreationDTO.content;
      editPosts.splice(editPosts.indexOf(editedPost), 1, clonePost);
      const postsAfterEdit = editPosts.slice();
      return { ...state, start: false, posts: postsAfterEdit };
    case EDIT_POST_ERROR:
      return { ...state, start: false, error: action.payload };
    default:
      return state;
  }
}
